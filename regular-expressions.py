import re
import sys, os

if len(sys.argv) < 2: 
	sys.exit("Please enter an argument to the program %s" % sys.argv[0])

filename = sys.argv[1]
if not os.path.exists(filename):
    sys.exit("error: file '%s' not found" % sys.argv[1])
f = open(filename)
regex = []
for line in f:
    regex.append(line.rstrip())
f.close()

#Regex 1
hello_world = re.compile(r"\bhello\sworld\b")
def find_all_hello_world(test):
    return hello_world.findall(test)

#Regex 2
triple_vowel = re.compile("\w*[aeiou]{3}\w*")
def find_all_triple_vowel(test):
    return triple_vowel.findall(test)

#Regex 3
flight_code = re.compile(r"\b([A-Z]{2}\d{3,4}){1}\b")
def find_all_flight_code(test):
    if(len(flight_code.findall(test)) <= 1):
        return flight_code.findall(test)
    else:
        return []

print(find_all_hello_world(regex[0]))
print(find_all_triple_vowel(regex[1]))
for i in range(2, len(regex)):
    print(find_all_flight_code(regex[i]))