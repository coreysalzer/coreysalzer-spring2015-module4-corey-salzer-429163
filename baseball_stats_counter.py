import re
import sys, os

#Player class keeps track of name, at_bats, and hits
class Player:
    def __init__(self, name):
        self.name = name
        self.at_bats = 0
        self.hits = 0
    
    def calculate_average(self):
        return round(float(self.hits)/float(self.at_bats), 3)

#sorts players by descending batting average
def sort_by_average(player1, player2):
    return cmp(player2.calculate_average(), player1.calculate_average())

#Regex string containing full name
player_name = re.compile(r"[a-zA-z]+\s[a-zA-z]+")
def find_player_name(test):
    match = player_name.match(test)
    if match is not None:
        return match.group()
    else:
        return False
    
#Regex returns array in format [at_bats, hits, runs] where
#at_bats, hits, and runs are strings
at_bats_hits_runs = re.compile(r"\d+")
def find_stats(test):
    return at_bats_hits_runs.findall(test)

#Gives usage message if command line argument is not present
if len(sys.argv) < 2: 
    sys.exit("Please enter an argument to the program %s" % sys.argv[0])

filename = sys.argv[1]
#Gives error message if the file given does not exist
if not os.path.exists(filename):
    sys.exit("error: file '%s' not found" % sys.argv[1])

#reads input line by line, excluding comments and game summary
f = open(filename)
lines = []
for line in f:
    if not line.startswith('#') | line.startswith('='): 
        lines.append(line.rstrip())
f.close()

#Gets information and stats about each player from each line
players = []
for line in lines:
    name = find_player_name(line)
    #skips over line if can't find name (i.e. blank lines)
    if not name == False:
        is_in_players = False
        
        #checks if player is already in the list of players and
        #gets index of the player in list
        for player in players:
            if player.name == name:
                player_index = players.index(player)
                is_in_players = True
        
        #adds player to the list if they are not already in the list
        if not is_in_players:
            temp_player = Player(name)
            players.append(temp_player)
            player_index = players.index(temp_player)
        
        #gets the at_bats, hits, and runs, and increments the player's
        #at_bats and hits to keep track of season total
        stats = find_stats(line)
        if not len(stats) == 3:
            sys.exit("error: at bats, hits, or runs not found")   
        players[player_index].at_bats = players[player_index].at_bats + int(stats[0])
        players[player_index].hits = players[player_index].hits + int(stats[1])

players.sort(sort_by_average)
for player in players:
    print("%s: %.3f" % (player.name, player.calculate_average()))