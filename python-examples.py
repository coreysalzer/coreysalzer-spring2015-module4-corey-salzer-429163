def my_function(name):
	print "Hello, %s!" % name

my_function("Batman")
my_function("Spiderman")

fruits = ["apple", "banana", "cherry", "date"]
for fruit in fruits:
	print "I love to eat fresh %s." % fruit

fruit_size = [len(fruit) for fruit in fruits]

avg_fruit_size = sum(fruit_size) / float(len(fruit_size))
print "The average fruit string length is %4.2f." % avg_fruit_size

#tuples
first_name, last_name = ("John", "Smith")

def compute_length(string):
	str_len = len(string)
	if str_len < 5:
		return (str_len, "short")
	elif str_len < 40:
		return (str_len, "medium")
	else: 
		return (str_len, "large")
length, description = compute_length("four score and seven years ago")
print "The %s string is %d characters long." % (description, length)

#dictionaries
fruits_in_bowl = {
	'apple': 4,
	'banana': 2,
	'cherry': 0,
	'date': 12
}

for fruit, num in fruits_in_bowl.items():
	print "There are %d %s(s) in the bowl." % (num, fruit)

#sorting

new_fruits = sorted(fruits, key=lambda v: len(v) )
print new_fruits

#import
import time

current_time = time.localtime()
print time.strftime('%a, %d %b %Y %H:%M:%S', current_time)

#file i/o
with open("example.txt", "w") as f:
	f.write("\tHello World")
f = open("example.txt")
file_contents = f.read()
print "File contents: %s" % file_contents
f.close()
f = open("example.txt")
for line in f:
	print "Read Line: %s" % line.rstrip()
f.close()

import sys, os

if len(sys.argv) < 2: 
	print "Please enter an argument to the program %s" % sys.argv[0]
else:
	filename = sys.argv[1]
	if not os.path.exists(filename):
		sys.exit("error: file '%s' not found" % sys.argv[1])
	f = open(filename)
	file_contents = f.read()
	print "%s contents: %s" % (sys.argv[1], file_contents)
	f.close()
	
#OOP
class Food:
	def __init__(self, name):
		self.name = name
	
	@staticmethod
	def get_definition():
		return "Food is nourishment for carbon-based lifeforms."
	
	def format_name(self):
		return "Gotta love to eat " + self.name
	
class Fruit(Food):
	def format_name(self):
		return Food.format_name(self) + " (fruit)"
	
fruit = Fruit("Cherry")
print fruit.format_name()
print Food.get_definition()
